FROM cyancor/psoc-build:latest
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

RUN apt-get update && apt-get install -y openssh-server g++ gdb gdbserver
RUN mkdir /var/run/sshd
RUN echo 'root:12345' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
COPY 10-ptrace.conf /etc/sysctl.d/10-ptrace.conf

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

RUN echo "export VISIBLE=now" >> /etc/profile

RUN export TOOLCHAIN_DIR="/arm/gcc-arm-none-eabi/bin"

COPY run.sh /run.sh
RUN chmod 0777 /run.sh

EXPOSE 22

CMD ["/run.sh"]